package com.novel.resource.oss.autoConfigurer;

import com.aliyun.oss.OSSClient;
import com.novel.resource.oss.config.OssConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * oss 配置类
 *
 * @author novel
 * @date 2019/6/4
 */
@Configuration
@ConditionalOnClass({OSSClient.class})
@EnableConfigurationProperties(OssConfig.class)
@Slf4j
public class OssAutoConfiguration {
    private final OssConfig ossConfig;

    public OssAutoConfiguration(OssConfig ossConfig) {
        this.ossConfig = ossConfig;
    }

    @Bean
    public OssClientFactoryBean ossClientFactoryBean() {
        OssClientFactoryBean factoryBean = new OssClientFactoryBean(ossConfig);
        log.info("OssClientFactoryBean 初始化...");
        return factoryBean;
    }
}
