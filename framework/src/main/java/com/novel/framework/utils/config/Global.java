package com.novel.framework.utils.config;

import com.novel.common.utils.StringUtils;
import com.novel.common.utils.config.GlobalUtil;

/**
 * 全局配置类
 *
 * @author novel
 * @date 2019/6/5
 */
public class Global {

    /**
     * 获取配置
     */
    public static String getConfig(String key) {
        return GlobalUtil.getConfig(key);
    }


    /**
     * 获取配置
     *
     * @param key          配置key
     * @param defaultValue 默认值
     * @return 配置值
     */
    public static String getConfig(String key, String defaultValue) {
        return GlobalUtil.getConfig(key, defaultValue);
    }

    /**
     * 设置全局属性
     *
     * @param key   属性key
     * @param value 属性值
     */
    public static String setConfig(String key, String value) {
        return GlobalUtil.setConfig(key, value);
    }

    /**
     * 获取项目版本
     */
    public static String getVersion() {
        String config = GlobalUtil.getConfig("project.projectInfo.version");
        return StringUtils.isNotEmpty(config) && !"null".equals(config) ? config : "0.0.1";
    }

    /**
     * 获取项目名称
     */
    public static String getName() {
        return StringUtils.nvl(GlobalUtil.getConfig("project.projectInfo.name"), "novel");
    }

    /**
     * 获取文件上传路径
     */
    public static String getProfile() {
        return GlobalUtil.getConfig("project.profile");
    }

    /**
     * 获取头像上传路径
     */
    public static String getAvatarPath() {
        return GlobalUtil.getConfig("project.profile") + "avatar/";
    }

    /**
     * 获取上传路径
     */
    public static String getUploadPath() {
        return GlobalUtil.getConfig("project.profile") + "upload/";
    }

}
