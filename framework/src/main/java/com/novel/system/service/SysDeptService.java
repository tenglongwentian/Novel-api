package com.novel.system.service;

import com.novel.system.domain.SysDept;
import com.novel.system.domain.SysRole;
import com.novel.system.domain.vo.TreeData;

import java.util.List;
import java.util.Map;

/**
 * 部门管理 服务层
 *
 * @author novel
 * @date 2019/5/15
 */
public interface SysDeptService {
    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    List<SysDept> selectDeptList(SysDept dept);

    /**
     * 查询部门表格树
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    List<SysDept> deptTreeTableData(SysDept dept);

    /**
     * 查询部门管理树
     *
     * @return 所有部门信息
     */
    List<Map<String, Object>> selectDeptTree();

    /**
     * 根据角色ID查询菜单
     *
     * @param role 角色对象
     * @return 菜单列表
     */
    List<Map<String, Object>> roleDeptTreeData(SysRole role);

    /**
     * 查询部门人数
     *
     * @param parentId 父部门ID
     * @return 结果
     */
    boolean selectDeptCount(Long parentId);

    /**
     * 查询部门是否存在用户
     *
     * @param id 部门ID
     * @return 结果 true 存在 false 不存在
     */
    boolean checkDeptExistUser(Long id);

    /**
     * 删除部门管理信息
     *
     * @param id 部门ID
     * @return 结果
     */
    boolean deleteDeptById(Long id);

    /**
     * 新增保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    boolean insertDept(SysDept dept);

    /**
     * 修改保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    boolean updateDept(SysDept dept);

    /**
     * 根据部门ID查询信息
     *
     * @param id 部门ID
     * @return 部门信息
     */
    SysDept selectDeptById(Long id);

    /**
     * 校验部门名称是否唯一
     *
     * @param dept 部门信息
     * @return 结果
     */
    String checkDeptNameUnique(SysDept dept);

    /**
     * 删除部门
     *
     * @param ids 部门id
     * @return 结果
     */
    boolean deleteMenuByIds(Long[] ids);

    /**
     * 查询部门树
     *
     * @return 结果
     */
    List<TreeData> deptTreeSelectData();
}
